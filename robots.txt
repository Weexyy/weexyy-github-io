User-agent: *
Allow: /
Allow: /p/
Allow: /post/
Allow: /archives/
Allow: /categories/
Allow: /tags/ 
Allow: /resources/
Disallow: /vendors/
Disallow: /js/
Disallow: /css/
Disallow: /fonts/
Disallow: /vendors/
Disallow: /fancybox/

Sitemap: https://blog.weexy.cn/sitemap.xml
Sitemap: https://blog.weexy.cn/baidusitemap.xml